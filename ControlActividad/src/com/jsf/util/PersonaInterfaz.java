package com.jsf.util;

import com.jsf.modelos.Persona;

public interface PersonaInterfaz extends Dao<Persona> {

	public Persona buscar(String nombre);

}
