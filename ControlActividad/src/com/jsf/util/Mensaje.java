package com.jsf.util;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.springframework.stereotype.Component;

@Component
@ManagedBean
@ViewScoped
public class Mensaje {
	public void ViewMessage(String mensaje) {
		FacesMessage men = new FacesMessage(mensaje);
		FacesContext.getCurrentInstance().addMessage(null, men);

	}

}
