package com.jsf.util;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public abstract class AbstractFacade<T> {

	Class<T> entityClass;

	public AbstractFacade(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	@Autowired /* para que es esta cosa */
	protected abstract SessionFactory sessionFactory();/* para que sirve */

	protected Session session;/* para que sirve */

	@Transactional /**/
	public void create(T entity) {
		try {
			session = sessionFactory().getCurrentSession();
			session.save(entity);
		} catch (Exception e) {
			System.out.println("Errro al insertar " + e);
			e.printStackTrace();
		}
	}

	@Transactional
	public void edit(T entity) {
		session = sessionFactory().getCurrentSession();
		session.update(entity);

	}

	@Transactional
	public void remove(T entity) {
		session = sessionFactory().getCurrentSession();
		session.delete(entity);
	}

	@Transactional
	public T find(Object id) {
		session = sessionFactory().getCurrentSession();
		return session.find(entityClass, id);

	}

	@Transactional
	public List<T> findAll() {
		session = sessionFactory().getCurrentSession();
		CriteriaQuery cq = session.getCriteriaBuilder().createQuery();
		cq.select(cq.from(entityClass));
		return session.createQuery(cq).getResultList();

	}

}
