package com.jsf.modelos;
// Generated 12-20-2019 09:25:14 AM by Hibernate Tools 5.2.12.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Fases generated by hbm2java
 */
@Entity
@Table(name = "fases", catalog = "actividades")
public class Fases implements java.io.Serializable {

	private Integer idFases;
	private String ingresarFecha;
	private Set actividadeses = new HashSet(0);
	private Set estados = new HashSet(0);

	public Fases() {
	}

	public Fases(String ingresarFecha, Set actividadeses, Set estados) {
		this.ingresarFecha = ingresarFecha;
		this.actividadeses = actividadeses;
		this.estados = estados;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id_fases", unique = true, nullable = false)
	public Integer getIdFases() {
		return this.idFases;
	}

	public void setIdFases(Integer idFases) {
		this.idFases = idFases;
	}

	@Column(name = "ingresar_fecha", length = 250)
	public String getIngresarFecha() {
		return this.ingresarFecha;
	}

	public void setIngresarFecha(String ingresarFecha) {
		this.ingresarFecha = ingresarFecha;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "fases")
	public Set getActividadeses() {
		return this.actividadeses;
	}

	public void setActividadeses(Set actividadeses) {
		this.actividadeses = actividadeses;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "fases")
	public Set getEstados() {
		return this.estados;
	}

	public void setEstados(Set estados) {
		this.estados = estados;
	}

}
