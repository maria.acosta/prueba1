package com.jsf.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import com.jsf.modelos.Persona;
import com.jsf.util.PersonaInterfaz;

@Component
@SessionScoped
@ManagedBean
public class PersonaManagedBean implements Serializable {

	public String refiere;

	public String getRefiere() {
		return refiere;
	}

	public void setRefiere(String refiere) {
		this.refiere = refiere;
	}

	private static final long serialVersionUID = 1L;

	@Autowired /**/
	@Qualifier("daoPersona")
	private PersonaInterfaz daoPersona;
	private String nombre;
	private List<Persona> personaList;
	Persona persona = new Persona();

	public PersonaInterfaz getDaoPersona() {
		return daoPersona;
	}

	public void setDaoPersona(PersonaInterfaz daoPersona) {
		this.daoPersona = daoPersona;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Persona> getPersonaList() {
		this.personaList = this.daoPersona.findAll();
		return personaList;
	}

	public void setPersonaList(List<Persona> personaList) {
		this.personaList = personaList;
	}
	
	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	@PostConstruct/**/
	public void init() {
		persona = new Persona();
		
	}

}
