<%-- 
    Document   : frutas
    Created on : 23-dic-2019, 23:10:15
    Author     : Teresa Acostha
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
    <center> <h1>Con un click debe de mostra las 10 frutas mas!</h1></center>

    <hr/>
    <img src="img/1.jpeg" alt=""/>
    <img src="img/2.jpeg" alt=""/>
    <img src="img/3.jpeg" alt=""/>
    <img src="img/4.jpeg" alt=""/>
    <img src="img/5.jpeg" alt=""/>
    <img src="img/6.jpeg" alt=""/>


    <button class="btn btn-success" onclick="mostrar()">Ver Todos!</button>
    <button class="btn btn-success" onclick="ocultar()">Ver Primeros!</button>
    <hr/>
    <div id="mostrar">
        <img src="img/1.jpeg" alt=""/>
        <img src="img/2.jpeg" alt=""/>
        <img src="img/3.jpeg" alt=""/>
        <img src="img/4.jpeg" alt=""/>
        <img src="img/5.jpeg" alt=""/>                                   
        <img src="img/6.jpeg" alt=""/>
        <img src="img/1.jpeg" alt=""/>
        <img src="img/2.jpeg" alt=""/>
        <img src="img/3.jpeg" alt=""/>
        <img src="img/4.jpeg" alt=""/>
        <img src="img/5.jpeg" alt=""/>
        <img src="img/6.jpeg" alt=""/>
        <img src="img/1.jpeg" alt=""/>
        <img src="img/2.jpeg" alt=""/>
        <img src="img/3.jpeg" alt=""/>
        <img src="img/4.jpeg" alt=""/>

    </div>
    <style>
        hr{
            background-color: #000;  

        }
        img{

            width: 200px;
        }

    </style>
    <script>

        'use strict'


        function mostrar() {

            document.getElementById('mostrar').style.display = "block";
        }

        function ocultar() {

            document.getElementById('mostrar').style.display = "none";
        }


    </script>
</body>
</html>
