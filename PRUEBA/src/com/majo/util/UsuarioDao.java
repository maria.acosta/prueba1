package com.majo.util;

import com.majo.modelos.Usuario;

public interface UsuarioDao extends Dao<Usuario> {
	public Usuario buscar(String nombre);

}
