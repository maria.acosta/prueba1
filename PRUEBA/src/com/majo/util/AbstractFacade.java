package com.majo.util;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public abstract class AbstractFacade<T> {
	private final Class<T> entityClass;

	public AbstractFacade(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	@Autowired
	protected abstract SessionFactory sessionFactory();

	protected Session session;

	@Transactional
	public void create(T entity) {
		try {
			session = sessionFactory().getCurrentSession();
			session.save(entity);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Transactional
	public void edit(T entity) {
		session = sessionFactory().getCurrentSession();
		session.update(entity);
	}

	@Transactional
	public void remove(T entity) {
		session = sessionFactory().getCurrentSession();
		session.delete(entity);

	}

	@Transactional
	public List<T> findAll() {
		session = sessionFactory().getCurrentSession();
		CriteriaQuery crq = session.getCriteriaBuilder().createQuery();
		crq.select(crq.from(entityClass));
		return session.createQuery(crq).getResultList();
	}

	@Transactional
	public T find(Object id) {
		session = sessionFactory().getCurrentSession();
		return session.find(entityClass, id);
	}

}
