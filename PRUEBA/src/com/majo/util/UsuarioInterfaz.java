package com.majo.util;

import com.majo.modelos.Usuario;

public interface UsuarioInterfaz extends Dao<Usuario>{
	
	public Usuario buscar(String nombre);

}
