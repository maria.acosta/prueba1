package com.majo.impl;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.SessionFactory;

import com.majo.modelos.Usuario;
import com.majo.util.AbstractFacade;
import com.majo.util.UsuarioInterfaz;

@Transactional
public class UsuarioDAO extends AbstractFacade<Usuario> implements UsuarioInterfaz {

	private SessionFactory sf;

	public UsuarioDAO(Class<Usuario> entityClass, SessionFactory sf) {
		super(entityClass);
		this.sf = sf;
	}

	@Override
	public Usuario buscar(String nombre) {
		Usuario u = new Usuario();
		CriteriaBuilder cb = sf.getCriteriaBuilder();
		CriteriaQuery<Usuario> cq = cb.createQuery(Usuario.class);
		Root<Usuario> usuario = cq.from(Usuario.class);
		cq.where(cb.equal(usuario.get("nombre"), nombre));

		for (Usuario usu : sf.getCurrentSession().createQuery(cq).getResultList()) {
			u.setId(usu.getId());
			u.setNombre(usu.getNombre());

		}
		System.out.print("Este es el id!!!!!!!!!!!!!!!! :" + u.getId());
		System.out.print("Este es el nombre del ususario!!!!!!!!!!!!!!!! :" + u.getNombre());

		return u;
	}

	@Override
	protected SessionFactory sessionFactory() {
		// TODO Auto-generated method stub
		return null;
	}

}
