package com.majo.controlles;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.inject.Qualifier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import com.majo.modelos.Usuario;
import com.majo.util.UsuarioInterfaz;

@Component
@SessionScope
@ManagedBean
public class UsuarioBean implements Serializable {
	private String refiere;

	public String getRefiere() {
		return refiere;
	}

	public void setRefiere(String refiere) {
		this.refiere = refiere;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	private UsuarioInterfaz daoUsuario;

	/* ID */
	private int idUsurario;

	/* Variables del Usuario */

	private String nombre;

	/* Extraccion de datos de usuario para consultar y consultarAll */

	private List<Usuario> UsuarioList;

	/* Instanciamos la clase correspondiente a usuario */

	Usuario usuario = new Usuario();

	/*
	 * Encapsulamos las variables declaradas privadas para poder acceder a ellas
	 * desde la clase
	 */

	public UsuarioInterfaz getDaoUsuario() {
		return daoUsuario;
	}

	public void setDaoUsuario(UsuarioInterfaz daoUsuario) {
		this.daoUsuario = daoUsuario;
	}

	public int getIdUsurario() {
		return idUsurario;
	}

	public void setIdUsurario(int idUsurario) {
		this.idUsurario = idUsurario;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Usuario> getUsuarioList() {
		return UsuarioList;
	}

	public void setUsuarioList(List<Usuario> usuarioList) {
		UsuarioList = usuarioList;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}
